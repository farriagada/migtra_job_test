#!/usr/bin/env python3
# encoding: utf-8

import pandas as pd
import numpy as np
from sklearn.neural_network import MLPRegressor


input_km = input("Ingrese la cantidad de kilómetros : ") 

data = pd.read_csv("fuel.csv")
x = data["Km"]
y = data["Fuel"]
X = x[:,np.newaxis] 

while True:
    # Entrenamiento de los datos
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    # Predicción
    mlr = MLPRegressor(
        solver='lbfgs', 
        alpha=1e-5, 
        hidden_layer_sizes=(3,3),
        random_state=1
        )
    mlr.fit(X_train, y_train)

    print(mlr.score(X_train, y_train))

    if mlr.score(X_train, y_train) > 0.95:
        """
        Terminar la ejecución del programa cuando el modelo
        tenga un score > 0.95 
        """
        break

print("Predicción en %s kilómetros : %s %% de combustible." %(int(input_km), mlr.predict(np.array([int(input_km)]).reshape(1, 1))))
