import json 

data1 = json.loads(open('data1.json').read())
data2 = json.loads(open('data2.json').read()) 


def get_statistics_1(data1):
    average = 0
    sum_var1 = 0
    sum_var2 = 0
    count_var1 = 0
    arr_var1 = []

    for region in data1:
        
        if region["name"] == "Region4":
            for province in region["children"]:
                for city in province["children"]:
                    if "var1" in city["values"]:
                        var1 = city["values"]["var1"]
                        arr_var1.append(var1)

        for province in region["children"]:

            if province["name"] == "Provincia2":
                for city in province["children"]:
                    if "var2" in city["values"]:
                        var2 = city["values"]["var2"]
                        sum_var2 += var2

            for city in province["children"]:
                if "var1" in city["values"]:
                    var1 = city["values"]["var1"]
                    sum_var1 += var1
                    count_var1 += 1

    average = sum_var1 / count_var1
    max_var1 = max(arr_var1)
    print("Solución problema 1")
    print("Promedio de la variable 'var1' : %s \nSuma de la variable 'var2' para la Provincia 2 : %s \nValor máximo de la variable 'var1' de la Región 4 : %s"  %(average, sum_var2, max_var1))


get_statistics_1(data1)



import datetime


def get_statistics_2(data2):

    await_time_a = 0
    await_time_b = 0
    count_a = 0
    count_b = 0

    for zone in data2:
        if zone["zone"] == "A":
            
            # IN

            dt_in_separate = zone["dt_in"]
            dt_in = dt_in_separate.split("T")
            dt_in_date_separate = dt_in[0].split("-")
            dt_in_time_separate = dt_in[1].split(":")

            year_in = int(dt_in_date_separate[0])
            month_in = int(dt_in_date_separate[1])
            day_in = int(dt_in_date_separate[2])
            hour_in = int(dt_in_time_separate[0])
            minute_in = int(dt_in_time_separate[1])
            second_in = int(dt_in_time_separate[2])

            # OUT
            
            dt_out_separate = zone["dt_out"]
            dt_out = dt_out_separate.split("T")
            dt_out_date_separate = dt_out[0].split("-")
            dt_out_time_separate = dt_out[1].split(":")

            year_out = int(dt_out_date_separate[0])
            month_out = int(dt_out_date_separate[1])
            day_out = int(dt_out_date_separate[2])
            hour_out = int(dt_out_time_separate[0])
            minute_out = int(dt_out_time_separate[1])
            second_out = int(dt_out_time_separate[2])

            dt_in_format = datetime.datetime(year_in,month_in,day_in,hour_in,minute_in, second_in)
            dt_out_format = datetime.datetime(year_out,month_out,day_out,hour_out,minute_out, second_out)
            await_time_a += (dt_in_format - dt_out_format).total_seconds() / 60
            count_a += 1

        elif zone["zone"] == "B":
            
            # IN

            dt_in_separate = zone["dt_in"]
            dt_in = dt_in_separate.split("T")
            dt_in_date_separate = dt_in[0].split("-")
            dt_in_time_separate = dt_in[1].split(":")

            year_in = int(dt_in_date_separate[0])
            month_in = int(dt_in_date_separate[1])
            day_in = int(dt_in_date_separate[2])
            hour_in = int(dt_in_time_separate[0])
            minute_in = int(dt_in_time_separate[1])
            second_in = int(dt_in_time_separate[2])

            # OUT
            
            dt_out_separate = zone["dt_out"]
            dt_out = dt_out_separate.split("T")
            dt_out_date_separate = dt_out[0].split("-")
            dt_out_time_separate = dt_out[1].split(":")

            year_out = int(dt_out_date_separate[0])
            month_out = int(dt_out_date_separate[1])
            day_out = int(dt_out_date_separate[2])
            hour_out = int(dt_out_time_separate[0])
            minute_out = int(dt_out_time_separate[1])
            second_out = int(dt_out_time_separate[2])

            dt_in_format = datetime.datetime(year_in,month_in,day_in,hour_in,minute_in, second_in)
            dt_out_format = datetime.datetime(year_out,month_out,day_out,hour_out,minute_out, second_out)
            await_time_b += (dt_in_format - dt_out_format).total_seconds() / 60
            count_b += 1


    average_a = round(abs(await_time_a / count_a))
    average_b = round(abs(await_time_b / count_b))
    percentage_w2 = 0
    count_percentage_w2 = 0

    for zone in data2:
        zone = zone["zone"]
        for word in zone:
            if word == "2":
                count_percentage_w2 += 1

    percentage_w2 = round(count_percentage_w2 * 100 / len(data2))
    print("\nSolución problema 2")
    print("Promedio de tiempo de espera en zonas A es : %s minutos " %(average_a))
    print("Promedio de tiempo de espera en zonas B es : %s minutos " %(average_b))
    print("Porcentaje de ciclos de faena que incluyeron alguna área de trabajo tipo 2 : %s %%" %(percentage_w2))


get_statistics_2(data2)

