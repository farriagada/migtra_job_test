# Introducción 

En este desafió se da solución a los problemas 1 y 2, además me tome la libertad de en el anunciado extra de análisis de datos agregar un análisis del porcentaje de combustible en relación a la cantidad de kilómetros recorridos por los camiones en las faenas, para esto cree un tercer dataset con datos ficticios `fuel.csv` el que tiene los datos de la cantidad de kilómetros recorridos y su porcentaje correspondiente de combustible. En este anunciado extra se determina el resultado mediante el entrenamiento de una red neuronal en un modelo predictivo.

## clonar repositorio
```shell
$ git clone git@gitlab.com:farriagada/migtra_job_test.git
$ cd migtra_job_test/
```

## Solución a problemas 1 y 2
```shell
$ python3 migtra.py
```

## Resultado 1 y 2
```shell
farriagada@221B:~/dev/migtra_job_test$ python migtra.py 
Solución problema 1
Promedio de la variable 'var1' : 11.1875 
Suma de la variable 'var2' para la Provincia 2 : 483 
Valor máximo de la variable 'var1' de la Región 4 : 21

Solución problema 2
Promedio de tiempo de espera en zonas A es : 13 minutos 
Promedio de tiempo de espera en zonas B es : 13 minutos 
Porcentaje de ciclos de faena que incluyeron alguna área de trabajo tipo 2 : 23 %
```


## Preparar entorno de desarrollo solución red neuronal
```shell
$ virtualenv -p python3 env
$ source env/bin/activate
$ pip install -r requirements.txt 
```

## Resultado modelo predictivo
```shell
(env) farriagada@221B:~/dev/migtra_job_test$ python3 fuel_prediction.py 
Ingrese la cantidad de kilómetros : 77
0.9084330077105086
0.9194159475531393
0.8974763700950258
0.8883905672333923
0.9025481457084674
0.8813694927171057
0.8939923599498706
0.8931249165968168
0.9076715186416262
0.8921726393713638
0.8820646323774316
0.8975121743955108
0.9044522320288806
0.902289828843626
0.892300948327796
0.9019608645050381
0.9994608147131425
Predicción en 77 kilómetros : [90.58926017] % de combustible.
```